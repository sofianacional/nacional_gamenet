﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] private Transform[] spawnPoints;
    public GameObject playerPrefab;

    private void Awake()
    {
        if(instance = null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady)
        {
            PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[Random.Range(0, spawnPoints.Length)].position, Quaternion.identity);
            
        }
    }

    public void RandomSpawn(GameObject player)
    {
        player.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;
    private int killCount;
    private string lastHit;

    [Header("Health")]
    public float startHp = 100;
    private float hp;
    public Image healthBar;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        hp = startHp;
        healthBar.fillAmount = hp / startHp;
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if(Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25); 
                // RPC = all players (and those who will soon join) will get the broadcast of the function
            }
        }
    }
    [PunRPC] // part 4 13:30
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.hp -= damage;
        this.healthBar.fillAmount = hp / startHp;

        if(hp <= 0)
        {
            Die(info);
            KillFeed(info);
        }
    }
    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }
    public void Die(PhotonMessageInfo info)
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
        else
        {
            killCount++;
            GameObject killCounterText = GameObject.Find("KillCounter");
            killCounterText.GetComponent<TextMeshProUGUI>().text = "Kill Count: " + killCount;
            lastHit = info.Sender.NickName;

            if(killCount == 10)
            {
                photonView.RPC("DisplayWinText", RpcTarget.AllBuffered, lastHit);

                // Disable controller
                // info.photonView.transform.GetComponent<PlayerMovementController>().enabled = false;
                // this.gameObject.GetComponent<PlayerMovementController>().enabled = false;
            }
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText");
        float respawnTime = 5.0f;

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You were killed. Respawning in " +
                respawnTime.ToString(".00") + " seconds";
        }
        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        GameManager.instance.RandomSpawn(this.gameObject);
        transform.GetComponent<PlayerMovementController>().enabled = true;
        animator.SetBool("isDead", false);

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }
    [PunRPC]
    public void RegainHealth()
    {
        hp = 100;
        healthBar.fillAmount = hp / startHp;
    }
    public void KillFeed(PhotonMessageInfo info)
    {
        GameObject killFeedText = GameObject.Find("KillFeed");
        killFeedText.GetComponent<TextMeshProUGUI>().text = 
            info.Sender.NickName + " killed " + info.photonView.Owner.NickName;
        StartCoroutine(DeleteKillFeed(killFeedText));
    }
    
    IEnumerator DeleteKillFeed(GameObject killFeedText)
    {
        yield return new WaitForSeconds(3.0f);
        killFeedText.GetComponent<TextMeshProUGUI>().text = "";
    }
    [PunRPC]
    public void DisplayWinText(string lastHit)
    {
        GameObject winText = GameObject.Find("WinText");
        winText.GetComponent<TextMeshProUGUI>().text = lastHit + " wins";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{
    [SerializeField] private Image healthBar;
    private float totalHp = 100;
    public float currentHp;

    void Start()
    {
        currentHp = totalHp;
        healthBar.fillAmount = currentHp / totalHp;
    }

    void Update()
    {
        
    }
    [PunRPC]
    public void TakeDamage(int damage)
    {
        currentHp -= damage;
        Debug.Log(currentHp);

        healthBar.fillAmount = currentHp / totalHp;
        if (currentHp <= 0)
        {
            Die();
        }
    }
    private void Die()
    {
        if(photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }
        
    }
}

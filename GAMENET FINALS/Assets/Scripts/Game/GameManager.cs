﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance = null;
    public GameObject[] characterPrefab;
    public Transform[] startingPositions;

    public GameObject countdownPanel;
    public Text timerText;

    public GameObject finishedPanel;
    public GameObject[] orderText;
    public string[] orderPlayerNames = new string[2];

    public GameObject listOfPlacesPanel;
    public Text[] placesText;

    #region Raise Event
    public enum RaiseEventCode
    {
        WhoFinishedEventCode = 0
    }
    public int finishOrder = 0;
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.WhoFinishedEventCode)
        {
            Debug.Log("everyone has finished");
            listOfPlacesPanel.SetActive(true);
            object[] data = (object[])photonEvent.CustomData;

            string[] playerNames = (string[])data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2];

            for(int i = 0; i <= PhotonNetwork.CurrentRoom.MaxPlayers; i++)
            {
                placesText[i].text = playerNames[i];
            }

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
    #endregion
    #region Unity Functions
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        if (PhotonNetwork.IsConnected)
        {
            object playerSelectionNumber;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Transform position = startingPositions[actorNumber - 1];
                Vector3 instantiatePosition = position.position;
                PhotonNetwork.Instantiate(characterPrefab[(int)playerSelectionNumber].name, instantiatePosition, position.rotation);

                countdownPanel.SetActive(true);
                listOfPlacesPanel.SetActive(false);
            }
        }
    }
    void Update()
    {

    }
    #endregion

    public void GameFinish(GameObject player)
    {
        player.GetComponent<PlayerSetup>().myCamera.transform.parent = null;
        player.GetComponent<PlayerMovement>().enabled = false;

        string nickname = player.GetComponent<PhotonView>().Owner.NickName;
        
        int viewId = player.GetComponent<PhotonView>().ViewID;

        player.GetComponent<PhotonView>().RPC("DisplayFinishedText", RpcTarget.AllBuffered, nickname);
        
        object[] data = new object[] { orderPlayerNames, finishOrder, viewId };

        if (finishOrder == 2) // all players have finished the course >> display finish order feed
        {
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);
        }
        
    }
}

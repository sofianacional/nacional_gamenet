﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class FinishLine : MonoBehaviourPunCallbacks
{
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player") && col.gameObject.GetComponent<PhotonView>().IsMine)
        {
            GameManager.instance.GameFinish(col.gameObject);
            Debug.Log("collided");
        }
    }
}

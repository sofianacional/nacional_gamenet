﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera myCamera;
    public GameObject fpsModel;
    public GameObject nonFpsModel;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<PlayerMovement>().enabled = photonView.IsMine;
        myCamera.enabled = photonView.IsMine;
        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);
        //GameManager.instance.finishLine.SetActive(photonView.IsMine);

    }
}

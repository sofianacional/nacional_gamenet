﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Finish : MonoBehaviourPunCallbacks
{
    void Start()
    {
        
    }
    void Update()
    {

    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.CompareTag("Player"))
        {
            GameManager.instance.GameFinish(col.gameObject);
            Debug.Log(col.gameObject);
        }
    }
}

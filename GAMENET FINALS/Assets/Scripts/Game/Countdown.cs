﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Countdown : MonoBehaviourPunCallbacks
{
    public GameObject countdownPanel;
    public Text timerText;
    public float timeToStartRace = 5.0f;

    void Start()
    {
        countdownPanel = GameManager.instance.countdownPanel;
        timerText = GameManager.instance.timerText;
    }

    void Update()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (timeToStartRace > 0)
            {
                timeToStartRace -= Time.deltaTime;
                photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartRace);
            }
            else if (timeToStartRace < 0)
            {
                photonView.RPC("StartRace", RpcTarget.AllBuffered);
            }
        }
    }
    [PunRPC]
    public void SetTime(float time)
    {
        if (time > 0)
        {
            timerText.text = time.ToString("F1");
        }
        else
        {
            timerText.text = "";
        }
    }
    [PunRPC]
    public void StartRace()
    {
        GetComponent<PlayerMovement>().isControlEnabled = true;
        this.enabled = false;
        countdownPanel.SetActive(false);
    }
}

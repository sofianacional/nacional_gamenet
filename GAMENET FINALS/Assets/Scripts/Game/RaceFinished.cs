﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class RaceFinished : MonoBehaviourPunCallbacks
{
    [PunRPC]
    public void DisplayFinishedText(string finishedPlayer)
    {
        GameManager.instance.finishOrder++;
        GameManager.instance.orderPlayerNames[GameManager.instance.finishOrder - 1] = finishedPlayer;
        Debug.Log(finishedPlayer + " finished. FinishOrder: " + GameManager.instance.finishOrder);

        GameObject ordertextUi = GameManager.instance.orderText[GameManager.instance.finishOrder - 1];
        ordertextUi.SetActive(true);
        
        ordertextUi.GetComponent<Text>().text = finishedPlayer + " finished";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CharacterSeclection : MonoBehaviour
{
    public GameObject[] characters;
    public int playerSelection;

    // Start is called before the first frame update
    void Start()
    {
        playerSelection = 0;
        ActivatePlayer(playerSelection);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void ActivatePlayer(int x)
    {
        foreach (GameObject go in characters)
        {
            go.SetActive(false);
        }

        characters[x].SetActive(true);

        // setting playing selection for vehicle
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable()
        { { Constants.PLAYER_SELECTION_NUMBER, playerSelection } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);
    }
    public void nextButtonClicked()
    {
        playerSelection++;
        if (playerSelection >= characters.Length)
        {
            playerSelection = 0;
        }
        ActivatePlayer(playerSelection);
        Debug.Log(playerSelection);
    }
    public void previousButtonClicked()
    {
        playerSelection--;
        if (playerSelection < characters.Length)
        {
            playerSelection = 0;
        }
        ActivatePlayer(playerSelection);
        Debug.Log(playerSelection);
    }
}

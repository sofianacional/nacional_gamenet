﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerListItemInitializer : MonoBehaviour
{
    public Text playerNameText;
    public Button playerReadyButton;
    public Image checkboxImage;

    private bool isPlayerReady = false;
    public void Initialize(int playerId, string playerName)
    {
        playerNameText.text = playerName;

        if (PhotonNetwork.LocalPlayer.ActorNumber != playerId)
        {
            Debug.Log("not mine");
            playerReadyButton.gameObject.SetActive(false);
        }
        else
        {
            playerReadyButton.gameObject.SetActive(true);
            ExitGames.Client.Photon.Hashtable initializeProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_READY, isPlayerReady } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(initializeProperties);

            playerReadyButton.onClick.AddListener(() =>
            {
                isPlayerReady = !isPlayerReady;
                SetPlayerReady(isPlayerReady);

                ExitGames.Client.Photon.Hashtable newProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_READY, isPlayerReady } };
                PhotonNetwork.LocalPlayer.SetCustomProperties(newProperties);
            });
        }
    }
    public void SetPlayerReady(bool playerReady)
    {
        checkboxImage.enabled = playerReady;

        if (playerReady)
        {
            playerReadyButton.GetComponentInChildren<Text>().text = "Unready";
        }
        else
        {
            playerReadyButton.GetComponentInChildren<Text>().text = "Ready";
        }
    }
}

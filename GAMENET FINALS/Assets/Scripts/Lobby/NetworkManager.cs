﻿// 03/29/21 Parkour Race Game Network Manager
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [Header("Panels")]
    public GameObject StartGamePanel;
    public GameObject InputNamePanel;
    public GameObject ConnectingPanel;
    public GameObject ChooseRoomPanel;
    public GameObject CreateRoomPanel;
    public GameObject InsideRoomPanel;

    [Header("InputNamePanel")]
    public InputField playerNameInput;

    [Header("GameOptionsPanel")]
    public InputField roomNameInput;

    [Header("InsideRoomPanel")]
    public Text roomNameText;
    public Text playerCountInRoomText;
    public GameObject playerListViewParent;
    public GameObject playerListPrefab;
    public GameObject startGameButton;

    public Dictionary<int, GameObject> playerListGameObjects;

    #region Unity Methods
    void Start()
    {
        StartGamePanel.SetActive(true);
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Update()
    {
        Debug.Log(PhotonNetwork.NetworkClientState);
    }
    #endregion
    #region UI Callback Methods
    public void OnStartButtonClicked()
    {
        InputNamePanel.SetActive(true);
    }
    public void OnLogInButtonClicked()
    {
        string playerName = playerNameInput.text;

        if (!string.IsNullOrEmpty(playerName))
        {
            ConnectingPanel.SetActive(true);

            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.LocalPlayer.NickName = playerName;
                PhotonNetwork.ConnectUsingSettings();
            }
        }
        else
        {
            Debug.Log("PlayerName is invalid!");
        }
    }
    public void OnChooseCreateRoomButtonClicked()
    {
        CreateRoomPanel.SetActive(true);
    }
    public void OnCreateRoomButtonClicked()
    {
        string roomName = roomNameInput.text;

        if (string.IsNullOrEmpty(roomName))
        {
            roomName = "Room " + Random.Range(1000, 10000);
        }

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 2;

        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    public void OnJoinRandomRoomButtonClicked()
    {
        InsideRoomPanel.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }
    public void OnStartGameButtonClicked()
    {
        PhotonNetwork.LoadLevel("GameScene");
    }
    #endregion
    #region Photon Callbacks
    public override void OnConnected()
    {
        Debug.Log("Connected to Internet");
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " is connected to Photon");
        ChooseRoomPanel.SetActive(true);
    }
    public override void OnCreatedRoom()
    {
        Debug.Log(PhotonNetwork.CurrentRoom.Name + " is created");
    }
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has joined " + PhotonNetwork.CurrentRoom.Name);
        InsideRoomPanel.SetActive(true);

        roomNameText.text = "Room " + PhotonNetwork.CurrentRoom.Name;
        playerCountInRoomText.text = "Player " + PhotonNetwork.CurrentRoom.PlayerCount + " of " + PhotonNetwork.CurrentRoom.MaxPlayers;

        if (playerListGameObjects == null)
        {
            playerListGameObjects = new Dictionary<int, GameObject>();
        }

        foreach (Player player in PhotonNetwork.PlayerList)
        {
            GameObject playerItem = Instantiate(playerListPrefab);
            playerItem.transform.SetParent(playerListViewParent.transform);
            playerItem.transform.localScale = Vector3.one;

            playerItem.GetComponent<PlayerListItemInitializer>().Initialize(player.ActorNumber, player.NickName);

            object isPlayerReady;
            if (player.CustomProperties.TryGetValue(Constants.PLAYER_READY, out isPlayerReady))
            {
                playerListPrefab.GetComponent<PlayerListItemInitializer>().SetPlayerReady((bool)isPlayerReady);
            }

            playerListGameObjects.Add(player.ActorNumber, playerItem);
        }
    }
    public override void OnPlayerEnteredRoom(Player player)
    {
        playerCountInRoomText.text = "Player " + PhotonNetwork.CurrentRoom.PlayerCount + " of " + PhotonNetwork.CurrentRoom.MaxPlayers;

        GameObject playerItem = Instantiate(playerListPrefab);
        playerItem.transform.SetParent(playerListViewParent.transform);
        playerItem.transform.localScale = Vector3.one;

        playerItem.GetComponent<PlayerListItemInitializer>().Initialize(player.ActorNumber, player.NickName);
        playerListGameObjects.Add(player.ActorNumber, playerItem);

        startGameButton.SetActive(CheckAllPlayerReady());
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(message);
        string roomName = Random.Range(1000, 10000).ToString();
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 2;

        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps) // called whnevever a property of the player changes
    {
        GameObject playerListGameObject;
        if (playerListGameObjects.TryGetValue(targetPlayer.ActorNumber, out playerListGameObject))
        {
            object isPlayerReady;
            if (changedProps.TryGetValue(Constants.PLAYER_READY, out isPlayerReady))
            {
                playerListGameObject.GetComponent<PlayerListItemInitializer>().SetPlayerReady((bool)isPlayerReady);
            }
        }
        startGameButton.SetActive(CheckAllPlayerReady());
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Destroy(playerListGameObjects[otherPlayer.ActorNumber].gameObject);
        playerListGameObjects.Remove(otherPlayer.ActorNumber);

        playerCountInRoomText.text = "Player " + PhotonNetwork.CurrentRoom.PlayerCount + " of " + PhotonNetwork.CurrentRoom.MaxPlayers;
    }
    public override void OnLeftRoom()
    {
        ChooseRoomPanel.SetActive(true);

        foreach (GameObject playerListGameObject in playerListGameObjects.Values)
        {
            Destroy(playerListGameObject);
        }

        playerListGameObjects.Clear();
        playerListGameObjects = null;
    }
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
        {
            startGameButton.SetActive(CheckAllPlayerReady());
        }
    }
    #endregion
    #region Private Methods
    private bool CheckAllPlayerReady()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            return false;
        }

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            object isPlayerReady;

            if (p.CustomProperties.TryGetValue(Constants.PLAYER_READY, out isPlayerReady))
            {
                if (!(bool)isPlayerReady)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        return true;
    }
    #endregion
    #region Public Methods

    #endregion

}

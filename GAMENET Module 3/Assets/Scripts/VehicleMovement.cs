﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public float speed = 20;
    public float rotationSpeed = 200;
    public float curentSpeed = 0;

    public bool isControlEnabled;

    // Start is called before the first frame update
    void Start()
    {
        isControlEnabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        if(isControlEnabled)
        {
            float translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
            float rotation = Input.GetAxis("Horizontal") * speed * Time.deltaTime;

            transform.Translate(0, 0, translation);
            curentSpeed = translation;

            transform.Rotate(0, rotation, 0);
        }
        
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = false;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<LapController>().enabled = false;
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = photonView.IsMine;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

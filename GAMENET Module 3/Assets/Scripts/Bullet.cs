﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviourPunCallbacks
{
	public int damage = 25;
    private void Start()
    {
		//photonView.GetComponentInParent<PhotonView>().RPC("SetValues", RpcTarget.AllBuffered);
    }
	[PunRPC]
	void SetValues()
    {
		damage = GetComponentInParent<Shooting>().damage;
	}
    void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.CompareTag("Player") && !col.gameObject.GetComponent<PhotonView>().IsMine)
		{
			Debug.Log(col.gameObject);
			col.gameObject.GetComponentInParent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
			Destroy(this.gameObject);
		}
		else if(col.gameObject.CompareTag("Map"))
        {
			Destroy(this.gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Shooting : MonoBehaviourPunCallbacks
{
    enum Weapon { laser, projectile }
    private Weapon curWeapon;
    public Camera camera;

    public GameObject turret;
    public GameObject bullet;
    public int damage = 25;

    public float curHp;
    public float maxHp = 100f;
    public Image healthBar;

    public string lastHit;
    public enum RaiseEventCode { WhoDied = 0, WhoWon = 1 };
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.WhoDied)
        {
            
            object[] data = (object[])photonEvent.CustomData;

            string nicknameOfDeadPlayer = (string)data[0];
            int viewId = (int)data[1];
            DeathRaceGameManager.instance.playerCounter = (int)data[2];

            if (viewId == photonView.ViewID)
            {
                DeathRaceGameManager.instance.deathScreenPanel.SetActive(true);
                DeathRaceGameManager.instance.playerName.text = "Player " + nicknameOfDeadPlayer;

                this.gameObject.SetActive(false);
            }

            if (DeathRaceGameManager.instance.playerCounter <= 1)
            {
                if (curHp > 0)
                {
                    photonView.RPC("DisplayWinner", RpcTarget.AllBuffered, photonView.Owner.NickName);
                    //DeathRaceGameManager.instance.winScreenPanel.SetActive(true);
                    //DeathRaceGameManager.instance.winnerNameText.text = photonView.Owner.NickName + " Won!";
                }
               
                this.gameObject.SetActive(false);
            }
        }
        else if(photonEvent.Code == (byte)RaiseEventCode.WhoWon)
        {
            object[] data = (object[])photonEvent.CustomData;
            int viewId = (int)data[0];

            if(curHp > 0)
            {
                photonView.RPC("DisplayWinner", RpcTarget.AllBuffered, photonView.Owner.NickName);

                //DeathRaceGameManager.instance.winScreenPanel.SetActive(true);
                //DeathRaceGameManager.instance.winnerNameText.text = photonView.Owner.NickName + " Won!";
                //this.gameObject.SetActive(false);
            }
            else 
            {
                DeathRaceGameManager.instance.deathScreenPanel.SetActive(false);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        curWeapon = Weapon.laser;
        photonView.RPC("SetHp", RpcTarget.AllBuffered);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot(curWeapon);
        }
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            weaponSwitch(curWeapon);
        }

        
    }
    void weaponSwitch(Weapon weapon)
    {
        if(weapon != Weapon.laser)
            curWeapon = Weapon.laser;
        else
            curWeapon = Weapon.projectile;
    }
    void Shoot(Weapon weapon)
    {
        if (weapon == Weapon.laser)
        {
            // for laser
            RaycastHit hit;
            Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

            if (Physics.Raycast(ray, out hit, 200))
            {
                Debug.Log(hit.collider.gameObject.name);

                if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
                {
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
                }
            }
        }
        else
        {
            // for projectile
            GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
            b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        }

        lastHit = photonView.Owner.NickName;
    }
    [PunRPC]
    public void SetHp()
    {
        curHp = maxHp;
    }
    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.curHp -= damage;
        this.healthBar.fillAmount = curHp / maxHp;

        if (curHp <= 0)
        {
            Die();
        }
    }
    public void Die()
    {
        DeathRaceGameManager.instance.playerCounter--;
        GetComponent<VehicleMovement>().enabled = false;
        GetComponent<PlayerSetup>().camera.transform.parent = null;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] { nickName, viewId, DeathRaceGameManager.instance.playerCounter };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions // realtime
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions // exit games
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WhoDied, data, raiseEventOptions, sendOptions);
    }
    
    public void Win()
    {
        GetComponent<VehicleMovement>().enabled = false;
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        int viewId = photonView.ViewID;

        object[] data = new object[] { lastHit, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions // realtime
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions // exit games
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WhoWon, data, raiseEventOptions, sendOptions);
    }
    [PunRPC]
    public void DisplayWinner(string winner)
    {
        //DeathRaceGameManager.instance.deathScreenPanel.SetActive(false);
        DeathRaceGameManager.instance.winScreenPanel.SetActive(true);
        DeathRaceGameManager.instance.winnerNameText.text = winner + " Won!";
        this.gameObject.SetActive(false);
    }
}
